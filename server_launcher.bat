@echo off
::
:: SouRCe Dedicated Server Launcher Helper - Josh 'Acecool' Moser
::


::
:: Config
::


::
:: Paths
::

:: Log Path ( This is where all of the console logs will be moved to if you choose to not delete them and condebug is enabled on client / server )
set LOGS_PATH=C:\Users\%UserName%\Dropbox\SRCDS_Logs\

:: Server Path - This should be where srcds.exe resides!
set SERVER_PATH=C:\Servers\srcds_acecooldev_framework

:: Garry's Mod Server Path
set GARRYSMOD_SERVER_PATH=%SERVER_PATH%\garrysmod

:: Garry's Mod Client Path - I'd recommend Creating a Steam Library in C:\SteamLibrary\ or C:\Games\SteamLibrary\ or something similar for simplicity...
set GARRYSMOD_CLIENT_PATH=C:\Program Files (x86)\Steam\steamapps\common\garrysmod\garrysmod

:: Garry's Mod Client autoexec_lastlocal filename ( you need to add exec autoexec_lastlocal.cfg to your autoexec.cfg file in cfg/
set GARRYSMOD_LASTLOCAL_PATH=%GARRYSMOD_CLIENT_PATH%\cfg\autoexec_lastlocal.cfg


::
:: Game Config
::

:: RCON Password
set RCON_PASS=SecreT123456TerceS

:: The Game-Mode in gamemodes/
set SERVER_GAMEMODE=base
REM set SERVER_GAMEMODE=sandbox
REM set SERVER_GAMEMODE=acecooldev_framework

:: Map the server should start on...
set SERVER_MAP=gm_construct
REM set SERVER_MAP=gm_flatgrass
REM set SERVER_MAP=rp_evocity_v33x

:: The AcecoolDev_Framework games/ game or gamemodes/ game you want to load on top of the framework.. Only active if SERVER_GAMEMODE is acecooldev_framework
set FRAMEWORK_GAME=tutorials

:: Workshop Collection ID
REM set WORKSHOP_COLLECTION=12345678
set WORKSHOP_COLLECTION=""

:: Workshop Authentication Key
REM set WORKSHOP_AUTHKEY=XXYYZZ112233
set WORKSHOP_AUTHKEY=""


::
:: Server Config
::

:: Server IP Address.. - NOTE: Wrong IP / Port can cause issues; sometimes even setting the correct LAN address can cause it not to connect!
REM set SERVER_IP=10.0.0.2
set SERVER_IP=""

:: Server Port.. - NOTE: Wrong IP / Port can cause issues; sometimes even setting the correct LAN address can cause it not to connect!
REM set SERVER_PORT=27015
set SERVER_PORT=27015

:: The Client Server Alias ( For the lastlocal alias in autoexec_lastlocal.cfg file ) - You can use aliases such as local ( if you define them; examples in garrysmod_client\ )
REM set SERVER_ALIAS=%SERVER_IP%:%SERVER_PORT%
set SERVER_ALIAS=localhost:%SERVER_PORT%
REM set SERVER_ALIAS=local

:: The password the server is using ( This will also be added to the lastlocal alias in autoexec_lastlocal.cfg file to simplify things )
set SERVER_PASS=""
REM set SERVER_PASS=!AcecoolDev_Framework!

:: Server FastDL URL.
REM set SERVER_FASTDL=http://localhost/fastdl/
set SERVER_FASTDL=""

:: Server Loading Screen URL.
REM set SERVER_LOADING_URL=http://localhost/garrysmod/new3.php?steamid=%s&map=%m
set SERVER_LOADING_URL=""

:: Server Error Reporting URL.
REM set SERVER_ERROR_REPORTING_URL=http://localhost/acecooldev_framework/error_reporting.php
set SERVER_ERROR_REPORTING_URL=""


::
:: Other Config
::

:: Should we make the server available only to our LAN ( This is required if you plan on connecting with more than one client on the same PC )?
set SERVER_LAN=false

:: Should we load the console ( This is required if you plan on connecting with more than one client on the same PC )
set SERVER_INSECURE=false

:: Should Server Cheats be Enabled?
set SERVER_CHEATS=false

:: Should we delete the logs instead of backing them up?
set DELETE_LOGS=false

:: Hide from the master server list?
set SERVER_NOMASTER=false

:: Should we load the console
set SERVER_CONSOLE=true

:: Should we log all console messages to a console.log file
set SERVER_CONDEBUG=true

:: Which game should the server be using...
set SERVER_GAME=garrysmod

:: Server Tick Rate
set SERVER_TICKRATE=33


::
:: ------------------------------------------------------------------------
::
::					...Config Ends / Script Begins...
::
:: ------------------------------------------------------------------------
::


::
:: Setup our backup file-name
::
set t=%time:~0,8%
set t=%t::=-%
set _filename=console_backup_%date%_%t%


::
:: Handle Console Logs by deleting or backing up and moving them...
::
echo Checking to see if console logs exist...

:: Client Console Log
if exist "%GARRYSMOD_CLIENT_PATH%\console.log" (
	if "%DELETE_LOGS%"=="true" (
		echo Deleting Clientside Console.log ( %GARRYSMOD_CLIENT_PATH%\console.log )
		del %GARRYSMOD_CLIENT_PATH%\console.log
	) else (
		echo Renaming Clientside Console.log to "%_filename%_client.log"
		rename "%GARRYSMOD_CLIENT_PATH%\console.log" "%_filename%_client.log"
		move "%GARRYSMOD_CLIENT_PATH%\%_filename%_client.log" "%LOGS_PATH%"
	)
) else (
	echo Clientside Console.log was not found in %GARRYSMOD_CLIENT_PATH% therefore it couldn't be moved or deleted..
)

:: Server Console Log
if exist "%GARRYSMOD_SERVER_PATH%\console.log" (
	if "%DELETE_LOGS%"=="true" (
		echo Deleting Serverside Console.log ( %GARRYSMOD_SERVER_PATH%\console.log )
		del %GARRYSMOD_SERVER_PATH%\console.log
	) else (
		echo Renaming Serverside Console.log to "%_filename%_client.log"
		rename "%GARRYSMOD_SERVER_PATH%\console.log" "%_filename%_server.log"
		move "%GARRYSMOD_SERVER_PATH%\%_filename%_server.log" "%LOGS_PATH%"
	)
) else (
	echo Serverside Console.log was not found in %GARRYSMOD_SERVER_PATH% therefore it couldn't be moved or deleted..
)


::
:: Update the Clientside autoexec_lastlocal file so the client can easily connect on launch or by typing lastlocal in console...
::
SET _data=alias lastlocal "password %SERVER_PASS%;%SERVER_ALIAS%"
>%GARRYSMOD_LASTLOCAL_PATH% ECHO %_data%


::
:: Set up Optionals
::

:: Server IP - NOTE: Wrong IP / Port can cause issues; sometimes even setting the correct LAN address can cause it not to connect!
if not "%SERVER_IP%"=="" (
	set _server_ip=-ip %SERVER_IP% 
)

:: Server Port - NOTE: Wrong IP / Port can cause issues; sometimes even setting the correct LAN address can cause it not to connect!
if not "%SERVER_PORT%"=="" (
	set _server_port=-port %SERVER_PORT% 
)

:: Server Console Active
if not "%SERVER_PASS%"=="" (
	set _pass=+sv_password %SERVER_PASS%
)

:: Server Console Active
if "%SERVER_CONSOLE%"=="true" (
	set _console=-console
)

:: Server Console Debugging Active
if "%SERVER_CONDEBUG%"=="true" (
	set _condebug=-condebug
)

:: Server Insecure
if "%SERVER_INSECURE%"=="true" (
	set _insecure=-insecure
)

:: Server Workshop Collection
if not "%WORKSHOP_COLLECTION%"=="" (
	set _collection=+host_workshop_collection %WORKSHOP_COLLECTION%
)

:: Server Authentication Key
if not "%WORKSHOP_AUTHKEY%"=="" (
	set _auth_key=-authkey %WORKSHOP_AUTHKEY%
)

:: Server Authentication Key
if "%SERVER_NOMASTER%"=="true" (
	set _nomaster=-nomaster
)

:: Server Authentication Key
if "%SERVER_LAN%"=="true" (
	set _sv_lan=+sv_lan 1
)

:: Server Authentication Key
if "%SERVER_CHEATS%"=="true" (
	set _sv_cheats=+sv_cheats 1
)

:: Server FastDL Server URL
if not "%SERVER_FASTDL%"=="" (
	set _fastdl=+sv_downloadurl %SERVER_FASTDL%
)

:: Server Loading Screen URL
if not "%SERVER_LOADING_URL%"=="" (
	set _loading_url=+sv_loadingurl %SERVER_LOADING_URL%
)

:: Server Error Reporting URL
if not "%SERVER_ERROR_REPORTING_URL%"=="true" (
	set _error_reporting=+lua_error_url %SERVER_ERROR_REPORTING_URL%
)



::
:: Assemble the executable and arguments...
REM ::
set _exe=%SERVER_PATH%\srcds.exe
set _exe_args= -game %SERVER_GAME% +exec server.cfg +r_hunkalloclightmaps 0 -tickrate %SERVER_TICKRATE% +rcon_password %RCON_PASS% +gamemode %SERVER_GAMEMODE% +sv_gamemode %FRAMEWORK_GAME% +map %SERVER_MAP% %_pass% %_sv_lan% %_sv_cheats%  %_server_ip% %_server_port% %_error_reporting% %_loading_url% %_fastdl% %_console% %_collection% %_auth_key% %_condebug% %_insecure% %_nomaster%

REM @echo on
REM echo %_exe_args%
REM @echo off

%_exe% %_exe_args%
pause
REM exit

