# AcecoolAHK_Framework README #

AcecoolAHK_Framework is an AutoHotkey framework which will support automatically loading of scripts from a neatly organized folder in your favorite Cloud backup folder...
It can now also launch executables and other files by dropping them into the addons_exe folder ( runs them outside of the framework in their own process; will execute any type of file ) - In order for these to load, you need to rebuild the framework loader files ( dialog asks you on launch ) and it will ask you whether you want to load or skip each file in this folder for security reasons ( Will soon add a hashing feature so you only need to whitelist an application once - may add automated virustotal search soon ).


### What is this repository for? ###

* An easy to use AutoHotkey script management solution which allows drag-and-drop integration for addon scripts and jobs / operations such as my Screenshot system ( Which captures a window, saves it with a useful name in Dropbox\Screenshots\ and creates a Windows notification letting you know the name with an OnClick function to open the screenshot and a dialog asking if you want to make the screenshot public by moving it into Dropbox\Public\Screenshots\ and giving you the public-direct-link to it to share )
* 0.4.20


### How do I get set up? ###

* This requires SRCDS to be installed prior to running; here is an easy ( pre-defined bat files and all ) to set it up: https://dl.dropboxusercontent.com/u/26074909/tutoring/server_srcds_steamcmd/setting_up_a_server_with_steamcmd.lua.html
* If you want to install FastDL on your Local SRCDS or Production Server, here's another walk-through: https://dl.dropboxusercontent.com/u/26074909/tutoring/server_srcds_steamcmd/fastdl_setup_instructions.lua.html
* If you don't want to generate a resources.lua file every single time your content changes, try this ( A script I wrote which recursively processes content located in gamemodes/<your_gm>/content/* and all of the *.gma files from addons/ ): https://dl.dropboxusercontent.com/u/26074909/tutoring/server_srcds_steamcmd/setting_up_downloads_using_recursive_resource_system.lua.html
* Note: All of my tutorials are listed as .lua.html for a reason; .html shows code in color but it is bad for copy/paste.. Remove .html from the URL to view straight-code!

* 1 ) Download the entire repository to your desktop or somewhere readily available!
* 2 ) Open <SteamLibraryPath>\steamapps\common\GarrysMod\garrysmod\cfg\autoexec.cfg ( create it if it doesn't exist ) for Editing!
* 2a) If you want to use the ip:port of the server dynamically and do not want to set up "local" aliases follow this step only ( otherwise use 2b ): Add to the end of the file
```lua
	exec autoexec_lastlocal.cfg
```
* 2b) If you want the option to use connection aliases instead of setting up the ip / port for lastlocal command - Copy the entire contents of: garrysmod_client\cfg\autoexec.cfg to the end of your <SteamLibraryPath>\steamapps\common\GarrysMod\garrysmod\cfg\autoexec.cfg ( create it if it doesn't exist ) file.
* 3 ) Copy garrysmod_client\cfg\autoexec_lastlocal.cfg to <SteamLibraryPath>\steamapps\common\GarrysMod\garrysmod\cfg\autoexec_lastlocal.cfg
* 4 ) Open server_launcher.bat for editing! We'll go through each option... Start below :: Config / :: Paths - The comments should be clear enough but I'll add additional info here..
* 4a) If you want to keep your server and / or client logs, set a path for LOGS_PATH.. Default is C:\Users\%UserName%\Dropbox\SRCDS_Logs\ ie C:\Users\Acecool\Dropbox\SRCDS_Logs\ - You will need to create the folder if it doesn't exist.
* 4b) Set up your SERVER_PATH which is where SRCDS.EXE will be after running SteamCMD and installing SRCDS - Default / Example path is: C:\Servers\srcds_acecooldev_framework
* 4c) Set up your Garry's Mod Server Path which should be in a nested-folder under srcds.exe with the name: garrysmod
* 4d) Set up your Clientside Garry's Mod Installation Path - Default should be correct for most users: C:\Program Files (x86)\Steam\steamapps\common\garrysmod\garrysmod
* 4e) If you renamed autoexec_lastlocal.cfg you'll need to update the filename here: GARRYSMOD_LASTLOCAL_PATH
* 4f) RCON_PASS is the password used to control your server - this needs to be changed. IF you want to disable it set it to "" ( with quotes so it looks like set RCON_PASS="" )
* 4g) SERVER_GAMEMODE: Set which Garry's Mod gamemodes\ Game-Mode you want to run such as darkrp, terrortown, base, sandbox, acecooldev_framework, etc...
* 4h) SERVER_MAP: Set up which map you want the server to start with such as gm_construct, gm_flatgrass, rp_evocity_v33x, etc..
* 4i) FRAMEWORK_GAME: This is only applicable if SERVER_GAMEMODE is acecooldev_framework; this is the framework games/ game or gamemodes/ game that will be loaded on top of the framework code.
* 4j) WORKSHOP_COLLECTION: This is for the workshop collection id that the server will automatically download and mount.
* 4k) WORKSHOP_AUTHKEY: You need a valid API Key in order for the server to download the workshop collection items.
* 4l) SERVER_IP: It is recommended you leave this alone; it is to force the ip address the server will use - it is easy to lock yourself out of the server though.
* 4m) SERVER_PORT: It is recommended you leave this alone - defaut is 27015 - only change this if you want to alter the port ( Go up in numbers )
* 4n) SERVER_ALIAS: This is what the client will call / join when using the command lastlocal...  you can type in the ip:port or use the alias such as local ( which you need to set up in autoexec.cfg )
* 4o) SERVER_PASS: The password to the server which is required for any user to join ( This is automatically added to the lastlocal alias so you won't need to type in a password when attempting to join your local srcds )
* 4p) SERVER_FASTDL: This is the FastD: URL where all your content is hosted on the web...
* 4q) SERVER_LOADING_URL: This url is what the client sees while they are connecting to your server...
* 4r) SERVER_ERROR_REPORTING_URL: This url can be used to capture Lua errors so you can have a locally controlled error management / reporting / tracking system...
* 4s) SERVER_LAN: Set this to true if you want the server to only be accessible to your local network ( Also required to be true if you want to connect multiple times with multiple garry's mod clients open on the same pc )
* 4t) SERVER_INSECURE: This disables Valve or Garry's Anti-Cheat if set to true! This is recommended for local dev servers and may also need to be true to join with multiple clients on the same pc.
* 4u) SERVER_CHEATS: If set to true, this will enable sv_cheats.
* 4v) DELETE_LOGS: If set to false, logs will be renamed / backed up and moved to the SERVER_LOGS path. If set to true, logs will be deleted when you launch your srcds..
* 4w) SERVER_NOMASTER: If set to true, your server will not report itself to the master server-list ( it will be hidden and may cause others to be unable to connect even if they have the ip; may also require server to be insecure )
* 4x) SERVER_CONSOLE: If set to true, a server-console will be opened for input / output for the duration the server is running - RECOMMENDED!
* 4y) SERVER_CONDEBUG: If set to true, console.log files will be created and will log everything in the console.
* 4z) SERVER_GAME: The game you want SRCDS to run such as garrysmod, cstrike, tf2, etc...
* 40) SERVER_TICKRATE: The number of ticks or server-"frames" executed per second on the server.




### Contribution guidelines ###

* Please ensure all submitted code follows the Acecool Company Code Standards to ensure uniformity.
* Please ensure all code is thoroughly tested prior to submission - If you need help you can either submit the idea using Issues tracker, or submit what code you have in a new pull-request and explain which part you need help with. Alternatively you may contact me on Discord: https://discord.gg/yk779zT
* Any code submitted must be your own or you must have permission to submit it. Plagiarism will not be tolerated. All submitted code will fall under the Acecool Company License ( Free to use, modify and learn from; original link must remain - may not resell without prior authorization )


### Who do I talk to? ###

* Josh 'Acecool' Moser
* Discord: https://discord.gg/yk779zT





::
:: Paths
::

:: Log Path ( This is where all of the console logs will be moved to )
set LOGS_PATH=C:\Users\Acecool\Dropbox\SRCDS_Logs\

:: Server Path
set SERVER_PATH=C:\AcecoolServers\srcds_acecooldev_framework

:: Garry's Mod Server Path
set GARRYSMOD_SERVER_PATH=%SERVER_PATH%\garrysmod

:: Garry's Mod Client Path - I'd recommend Creating a Steam Library in C:\SteamLibrary\ or C:\Games\SteamLibrary\ or something similar for simplicity...
set GARRYSMOD_CLIENT_PATH=C:\Program Files (x86)\Steam\steamapps\common\garrysmod\garrysmod

:: Garry's Mod Client autoexec_lastlocal filename ( you need to add exec autoexec_lastlocal.cfg to your autoexec.cfg file in cfg/
set GARRYSMOD_LASTLOCAL_PATH=%GARRYSMOD_CLIENT_PATH%\cfg\autoexec_lastlocal.cfg


::
:: Game Config
::

:: RCON Password
set RCON_PASS=SecreT123456TerceS

:: The Game-Mode in gamemodes/
set SERVER_GAMEMODE=base
REM set SERVER_GAMEMODE=sandbox
REM set SERVER_GAMEMODE=acecooldev_framework

:: Map the server should start on...
set SERVER_MAP=gm_construct
REM set SERVER_MAP=gm_flatgrass
REM set SERVER_MAP=rp_evocity_v33x

:: The AcecoolDev_Framework games/ game or gamemodes/ game you want to load on top of the framework.. Only active if SERVER_GAMEMODE is acecooldev_framework
set FRAMEWORK_GAME=tutorials

:: Workshop Collection ID
REM set WORKSHOP_COLLECTION=12345678
set WORKSHOP_COLLECTION=""

:: Workshop Authentication Key
REM set WORKSHOP_AUTHKEY=XXYYZZ112233
set WORKSHOP_AUTHKEY=""


::
:: Server Config
::

:: Server IP Address.. - NOTE: Wrong IP / Port can cause issues; sometimes even setting the correct LAN address can cause it not to connect!
REM set SERVER_IP=10.0.0.2
set SERVER_IP=""

:: Server Port.. - NOTE: Wrong IP / Port can cause issues; sometimes even setting the correct LAN address can cause it not to connect!
REM set SERVER_PORT=27015
set SERVER_PORT=""

:: The Client Server Alias ( For the lastlocal alias in autoexec_lastlocal.cfg file ) - You can use aliases such as local ( if you define them; examples in garrysmod_client\ )
set SERVER_ALIAS=%SERVER_IP%:%SERVER_PORT%
REM set SERVER_ALIAS=local

:: The password the server is using ( This will also be added to the lastlocal alias in autoexec_lastlocal.cfg file to simplify things )
set SERVER_PASS=""
REM set SERVER_PASS=!AcecoolDev_Framework!

:: Server FastDL URL.
REM set SERVER_FASTDL=http://localhost/fastdl/
set SERVER_FASTDL=""

:: Server Loading Screen URL.
REM set SERVER_LOADING_URL=http://localhost/garrysmod/new3.php?steamid=%s&map=%m
set SERVER_LOADING_URL=""

:: Server Error Reporting URL.
REM set SERVER_ERROR_REPORTING_URL=http://localhost/acecooldev_framework/error_reporting.php
set SERVER_ERROR_REPORTING_URL=""


::
:: Other Config
::

:: Should we make the server available only to our LAN ( This is required if you plan on connecting with more than one client on the same PC )?
set SERVER_LAN=false

:: Should we load the console ( This is required if you plan on connecting with more than one client on the same PC )
set SERVER_INSECURE=false

:: Should Server Cheats be Enabled?
set SERVER_CHEATS=false

:: Should we delete the logs instead of backing them up?
set DELETE_LOGS=false

:: Hide from the master server list?
set SERVER_NOMASTER=false

:: Should we load the console
set SERVER_CONSOLE=true

:: Should we log all console messages to a console.log file
set SERVER_CONDEBUG=true

:: Which game should the server be using...
set SERVER_GAME=garrysmod

:: Server Tick Rate
set SERVER_TICKRATE=33